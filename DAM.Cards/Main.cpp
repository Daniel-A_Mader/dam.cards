enum class Rank
{
	Ace,
	King,
	Queen,
	Jack,
	Ten,
	Nine,
	Eight,
	Seven,
	Six,
	Five,
	Four,
	Three,
	Two,
};
enum class Suit
{
	Diamonds,
	Hearts,
	Spades,
	Clubs
};
struct Cards
{
	Rank R;
	Suit S;
};
